# Summary

[01 Python Intro](./01_python_intro.md)
[02_pydocs_pep8](./02_pydocs_pep8.md)
[03_objects](./03_objects.md)
[04_py2_py3](./04_py2_py3.md)
[05_running_python](./05_running_python.md)
[LAB_1a](./lab1a.md)
[ReadMe](./README.md)